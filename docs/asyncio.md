# Python Asyncio: 코루틴, 태스크 및 이벤트

## <a name="intro"></a> 개요
코루틴, 태스크 및 이벤트를 사용하여 비동기 코드를 작성할 수 있는 모듈인 Python Asyncio에 대한 이 튜토리얼에서 설명하는 내용은 다음과 같다.

- 비동기 프로그래밍이란 무엇이며, 데이터 분석과 웹 개발에 유용한 이유
- Asyncio가 Python에서 작동하는 방식과 모듈의 주요 구성 요소
- Asyncio의 기본 구성 요소인 코루틴의 생성과 실행
- 태스크를 사용하여 코루틴의 동시 실행과 관리
- 이벤트를 사용하여 이벤트 루프 제어와 콜백 처리
- 웹 스크래핑, 웹 크롤링 및 웹 소켓 같은 일반적인 시나리오에 비동기 적용

시작하기 전에 시스템에 Python 3.7 이상이 설치되어 있어야 한다. 이는 Asyncio는 Python에 내장된 모듈이기 때문이다. 변수, 데이터 타입, 함수, 루프 및 모듈 같은 Python과 데이터 분석에 대한 기본 지식도 필요하다. 이러한 주제에 대해 학습이 필요한 경우 [Python for Data Analysis 코스](https://wesmckinney.com/book/)를 참고하세요.

Asyncio에 뛰어들 준비가 되었나요? 시작해봅시다!

## <a name="sec_02"></a> 비동기 프로그래밍이란?
이 절에서는 비동기 프로그래밍이 무엇인지, 왜 데이터 분석과 웹 개발에 유용한지 설명할 것이다. 또한 동기 코드와 비동기 코드의 차이점과 각각의 코드 사용 시기를 파악하는 방법도 보일 것이다.

비동기 프로그래밍은 여러 작업을 서로 차단하지 않고 동시에 실행할 수 있도록 하는 코드 작성 방법이다. 이는 다른 작업을 시작하기 전에 한 작업이 끝날 때까지 기다리지 않고 여러 작업을 동시에 수행할 수 있음을 의미한다. 이는 특히 네트워크 요청, 데이터베이스 쿼리 또는 파일 작업 같이 외부 자원을 기다리는 태스크를 처리할 때 코드의 성능과 효율성을 향상시킬 수 있다.

예를 들어, 여러 웹 사이트에서 데이터를 수집하는 웹 스크래퍼를 작성하고 있다고 가정해 보자. 코드를 동기식으로 작성하면 각 웹 사이트가 로드되고 처리될 때까지 기다렸다가 다음 웹 사이트로 이동해야 한다. 이 작업에는 많은 시간과 리소스가 필요하고 프로그램이 대부분 유휴(idle) 상태가 될 것이다. 그러나 코드를 비동기식으로 작성하면 여러 웹 사이트에 동시에 요청을 보낼 수 있고, 나머지 프로그램을 차단하지 않고도 데이터가 도착하는 대로 처리할 수 있다. 이렇게 하면 프로그램이 더 빠르고 응답성이 높아지며 프로그램이 대부분 바쁜(busy) 상태가 될 수 있다.

동기 코드와 비동기 코드의 차이는 다음과 같은 비유로 설명될 수 있다. 당신이 식당에 있고 피자를 주문하고 싶다고 해자. 만약 여러분이 동기식으로 주문한다면, 여러분은 피자가 요리되고 배달되기를 기다리며 책을 읽거나, 핸드폰을 확인하거나, 친구들과 이야기하는 것과 같은 다른 것을 할 수 있다. 그러나 만약 여러분이 비동기식으로 주문한다면, 여러분의 피자가 준비되고 배달되는 동안 여러분은 다른 것을 할 수 있고, 여러분은 피자가 준비되었 때 알림을 받을 것이다. 이렇게 하면, 여러분의 시간과 자원을 더 잘 활용할 수 있고, 그리고 마침내 피자가 도착했을 때 파자를 즐길 수 있다.

그렇다면 동기 코드 또는 비동기 코드를 언제 사용해야 하는지 어떻게 알 수 있는가? 일반적인 규칙은 CPU 바인딩된 태스크에는 동기 코드를 사용해야 한다는 것이다. 이는 많은 계산과 처리 능력을 필요로 한다는 것을 의미하며, I/O 바인딩된 태스크에는 비동기 코드를 사용해야 한다는 것이며, 이는 외부 소스로부터의 입력 또는 출력을 대기하는 것을 의미한다. 예를 들어 리스트 정렬, 수학적 함수 계산, 메시지 암호화 등의 태스크에는 동기 코드를 사용해야 하며, 파일 다운로드, 데이터베이스 쿼리, 이메일 전송 등의 태스크에는 비동기 코드를 사용해야 한다.

다음 절에서는 코루틴, 태스크 및 이벤트를 관리하기 위한 고급 인터페이스를 제공하는 `Asyncio` 모듈을 사용하여 Python에서 비동기 코드를 작성하는 방법을 설명할 것이다.

## <a name="sec_03"></a> Python에서 Asyncio 작동 방식
이 절에서는 Python에서 Asyncio가 어떻게 작동하는지, 그리고 모듈의 주요 구성 요소에 대하여 설명한다. 또한 `asyncio.run()` 함수를 사용하여 간단한 Asyncio 프로그램을 만들고 실행하는 방법도 보인다.

Asyncio는 Python으로 비동기 코드를 작성하기 위한 상위 수준의 인터페이스를 제공하는 모듈이다. 여러 태스크의 실행을 동시에 실행하고 관리하는 메커니즘인 이벤트 루프(event loop) 개념을 기반으로 한다. 태스크는 이벤트 루프에 의해 스케줄링되고 실행될 수 있는 작업 단위이다. 특정 지점에서 실행을 일시 중지했다가 재개할 수 있는 특수한 형태의 함수인 코루틴으로부터 태스크를 생성할 수 있으며, 그 사이에 다른 태스크를 실행할 수 있다. 코루틴은 비동기 연산의 결과를 나타내는 객체인 다른 코루틴이나 future<sup>[1](#footnote_1)</sup>의 결과를 기다릴 수 있다.

이벤트 루프는 태스크를 실행하고, 이벤트를 처리하며, 태스크와 외부 자원 사이의 통신을 조정하는 역할을 한다. 이벤트는 네트워크 요청, 타이머, 키보드 입력, 신호 등 프로그램이 통제할 수 없는 범위 밖에서 일어나는 모든 것을 지칭한다. 이벤트 루프는 이벤트와 실행할 준비가 된 작업을 저장하기 위해 대기열을 사용한다. 그런 다음 대기열이 비어 있거나 프로그램이 중단될 때까지 차단하지 않는 방식으로 하나씩 처리한다.

Asyncio를 사용하려면 모듈을 가져와 이벤트 루프 객체를 생성해야 한다. 그런 다음 `asyncio.run()` 함수를 사용하여 프로그램의 주 진입점으로 코루틴을 실행할 수 있다. `asyncio.run()` 함수는 새 이벤트 루프를 생성하고 완료될 때까지 코루틴을 실행한 다음 루프를 닫는다. 예를 들어 다음 코드는 1초 후에 `"Hello, world!"`를 출력하는 간단한 코루틴을 생성하고 실행한다.

```python
# Import the asyncio module
import asyncio

# Define a coroutine
async def hello():
    # Wait for one second
    await asyncio.sleep(1)
    # Print a message
    print("Hello, world!")

# Run the coroutine
asyncio.run(hello())
```

위의 코드를 실행하면 다음과 같은 출력을 디스플레이한다.

```
Hello, world!
```

다음 섹션에서는 코루틴, 코루틴 생성과 실행 그리고 `async`와 `await` 키워드 사용에 대해 자세히 알아본다.

## <a name="sec_04"></a> 코루틴: Asyncio의 기본 구성요소
이 절에서는 코루틴, 코루틴 생성 및 실행 방법, `async`와 `await` 키워드 사용 방법에 대해 자세히 알아본다. 또한 코루틴에서 오류와 예외를 처리하는 방법, 코루틴 취소와 시간 제한 방법에 대해 알아본다.

코루틴은 특정 지점에서 실행을 일시 중지했다가 재개할 수 있는 특수한 형태의 함수로, 그 사이에 다른 태스크를 실행할 수 있도록 해준다. 코루틴은 비동기 연산의 결과를 나타내는 객체인 다른 코루틴이나 future의 결과를 기다릴 수 있다. Coroutine은 비동기 코드를 순차적이고 가독적인 방식으로 작성할 수 있게 해주므로 Asyncio의 기본 구성 요소이다.

코루틴을 만들려면 `def` 키워드 앞에 `async` 키워드를 사용해야 한다. 이는 함수가 코루틴이며 함수 내부에서 `await` 키워드를 사용할 수 있음을 나타낸다. 예를 들어, 다음 코드는 주어진 지연 후에 메시지를 출력하는 코루틴을 정의한다.

```python
# Import the asyncio module
import asyncio

# Define a coroutine
async def say_after(delay, message):
    # Wait for the delay
    await asyncio.sleep(delay)
    # Print the message
    print(message)
```

코루틴을 실행하려면 `await` 키워드를 다른 코루틴 내부에 사용하거나 `asyncio.run()` 함수를 사용해야 합니다. `await` 키워드는 대기 중인 코루틴 또는 향후 완료될 때까지 현재 코루틴의 실행을 일시 중지한 다음 결과와 함께 다시 시작한다. 예를 들어, 다음 코드는 여러 코루틴 또는 향후 결과를 집계하여 반환하는 `asyncio.gather()` 함수를 사용하여 두 개의 코루틴을 동시에 실행한다.

```python
# Import the asyncio module
import asyncio

# Define a coroutine
async def say_after(delay, message):
    # Wait for the delay
    await asyncio.sleep(delay)
    # Print the message
    print(message)

# Define another coroutine
async def main():
    # Run two coroutines concurrently and wait for their results
    await asyncio.gather(
        say_after(1, "Hello"),
        say_after(2, "World")
    )

# Run the main coroutine
asyncio.run(main())
```

위의 코드를 실행하면 다음과 같은 출력이 디스플레이된다.

```
Hello
World
```

출력의 순서는 코루틴들의 순서와 상이할 수 있다. 이는 코루틴은 동시에 그리고 독립적으로 실행되기 때문이다. `asyncio.gather()` 함수는 결과를 반환하기 전에 두 코루틴들이 끝날 때까지 기다린다.

다음 섹션에서는 태스크를 사용하여 코루틴을 동시에 실행하고 그을 관리하는 방법에 대해 알아본다.

## <a name="sec_05"></a> 태스크: 코루틴 동시 실행
이 절에서는 태스크를 사용하여 코루틴을 동시에 실행하고 실행을 관리하는 방법을 설명한다. 또한 태스크를 생성, 취소 및 대기하는 방법과 `asyncio.create_task()`와 `asyncio.gather()` 함수 사용 방법도 보인다.

태스크(task)는 코루틴을 감싸고 이벤트 루프에서 실행하는 future의 하위 클래스이다. 태스크는 대기 중, 실행 중, 완료, 취소 등 코루틴의 실행 상태를 나타내는 객체이다. 태스크는 코루틴의 결과 또는 예외를 저장할 수도 있고, 완료되면 다른 태스크 또는 future에 알릴 수도 있다.

태스크를 생성하려면 코루틴을 인수로 사용하고 태스크 객체를 반환하는 `asyncio.create_task()` 함수를 사용할 수 있다. 그런 다음 `await` 키워드를 사용하여 태스크가 완료될 때까지 기다리거나 `asyncio.gather()` 함수를 사용하여 여러 태스크를 동시에 실행하고 결과를 기다릴 수 있다. 예를 들어 다음 코드는 `asyncio.create_task()` 함수를 사용하여 두 태스크를 생성하고 실행한 다음 `asyncio.gather()` 함수를 사용하여 태스크를 기다린다.

```python
# Import the asyncio module
import asyncio

# Define a coroutine
async def say_after(delay, message):
    # Wait for the delay
    await asyncio.sleep(delay)
    # Print the message
    print(message)

# Define another coroutine
async def main():
    # Create two tasks from coroutines
    task1 = asyncio.create_task(say_after(1, "Hello"))
    task2 = asyncio.create_task(say_after(2, "World"))
    # Run the tasks concurrently and wait for their results
    await asyncio.gather(task1, task2)

# Run the main coroutine
asyncio.run(main())
```

위의 코드를 실행하면 다음과 같이 출력된다.

```
Hello
World
```

`asyncio.create_task()` 함수는 현재 이벤트 루프에서 태스크를 직접 생성하고 스케줄링하는 것임에 유의한다. 또한 `loop.create_task()` 메서드를 사용하여 명시적으로 태스크를 생성할 수도 있다.

다음 절에서는 이벤트를 사용하여 이벤트 루프를 제어하고 콜백을 처리하는 방법에 대해 알아 볼 것이다.

## <a name="sec_06"></a> 이벤트: 이벤트 루프와 콜백 관리
이 절에서는 이벤트를 사용하여 이벤트 루프를 제어하고 콜백을 처리하는 방법을 설명한다. 또한 `asyncio.get_event_loop()`와 `asyncio.set_event_loop()` 함수를 사용하는 방법과 `loop.call_soon()`, `loop.call_later()` 및 `loop.call_at()` 메서드를 사용하는 방법도 보인다.

이벤트는 네트워크 요청, 타이머, 키보드 입력, 신호 등 프로그램이 통제할 수 없는 범위 밖에서 일어나는 모든 일을 말한다. 이벤트 루프는 태스크와 이벤트의 실행을 실행하고 관리하기 때문에 비동기의 핵심 구성 요소이다. 이벤트 루프는 큐를 사용하여 이벤트와 실행할 준비가 된 태스크를 저장한다. 그런 다음 큐가 비어 있거나 프로그램이 중단될 때까지 하나씩 차단하지 않는 방식으로 처리한다.

이벤트 루프를 사용하려면 현재 컨텍스트에 대한 현재 이벤트 루프를 반환하는 `asyncio.get_event_loop()` 함수를 사용하여 이벤트 루프에 대한 참조를 얻어야 한다. 또한 이벤트 루프 객체를 인수로 사용하는 `asyncio.set_event_loop()` 함수를 사용하여 현재 컨텍스트에 대한 새로운 이벤트 루프를 설정할 수 있다. 예를 들어, 다음 코드는 새로운 이벤트 루프를 생성하고 현재 이벤트 루프로 설정한다.

```python
# Import the asyncio module
import asyncio

# Create a new event loop
loop = asyncio.new_event_loop()
# Set the new event loop as the current one
asyncio.set_event_loop(loop)
```

이벤트 루프에 대한 참조가 있으면 이를 사용하여 태스크와 이벤트를 스케줄링할 수 있다. 이벤트를 스케줄링하는 한 가지 방법은 이벤트가 발생했을 때 호출되는 함수인 콜백을 사용하는 것이다. `loop.call_soon()` 메서드를 사용하여 최대한 빨리 실행되는 콜백을 스케줄링할 수 있고, `loop.call_later()` 메서드를 사용하여 주어진 시간 후에 실행되는 콜백을 스케줄링할 수 있으며, `loop.call_at()` 메서드를 사용하여 주어진 절대 시간에 실행되는 콜백을 스케줄링할 수 있다. 예를 들어, 다음 코드는 이 방법들을 사용하여 3개의 콜백을 스케줄링한다.

```python
# Import the asyncio module
import asyncio

# Define a callback function
def hello(name):
    # Print a message
    print(f"Hello, {name}!")

# Get the current event loop
loop = asyncio.get_event_loop()
# Schedule a callback to be executed as soon as possible
loop.call_soon(hello, "Alice")
# Schedule a callback to be executed after 2 seconds
loop.call_later(2, hello, "Bob")
# Schedule a callback to be executed at a given absolute time
loop.call_at(loop.time() + 4, hello, "Charlie")
# Run the event loop until all callbacks are done
loop.run_forever()
```

위의 코드를 실행하면 다음과 같은 결과를 출력한다.

```
Hello, Alice!
Hello, Bob!
Hello, Charlie!
```

`loop.run_forever()` 메서드는 `loop.stop()` 메서드를 호출하거나 예외적으로 이벤트 루프를 중지할 때까지 실행된다. 또한 `loop.run_til_complete()` 메서드를 사용하여 주어진 작업이나 future가 완료될 때까지 이벤트 루프를 실행할 수 있다.

다음 절에서는 웹 스크래핑, 웹 크롤링, 웹 소켓과 같은 일반적인 시나리오에 Asyncio를 적용하는 방법을 설명할 것이다.

## <a name="sec_07"></a> 예: 일반적인 시나리오에서 Asyncio 사용
이 절에서는 웹 스크래핑, 웹 크롤링, 웹 소켓과 같은 일반적인 시나리오에 Asyncio를 적용하는 방법을 설명한다. 또한 이러한 작업을 효율적으로 동시에 수행하기 위해 Asyncio를 사용하는 방법을 보여주는 코드 예도 들을 것이다.

웹 스크래핑은 일반적으로 데이터 분석, 정보 검색 또는 콘텐츠 집계를 목적으로 웹 페이지에서 데이터를 추출하는 과정이다. 웹 스크래핑은 requests, BeautifulSoup, Scrapy 또는 Selenium 같은 다양한 도구와 라이브러리를 사용하여 수행할 수 있다. 그러나 웹 스크래핑은 특히 웹 페이지의 수가 많거나 요청 비율이 높은 경우 웹 스크래퍼의 성능과 확장성을 향상시킬 수 있는 Asyncio를 사용하여 수행할 수 있다.

웹 스크래핑에 Asyncio를 사용하려면 HTTP 요청과 응답을 비동기적으로 주고받을 수 있는 `aiohttp` 같은 비동기 HTTP 클라이언트 라이브러리를 사용해야 한다. 또한 HTML 파일을 비동기적으로 읽고 구문 분석할 수 있는 `aiofiles` 같은 비동기 HTML 파서 라이브러리를 사용해야 한다. 그런 다음 코루틴과 태스크를 사용하여 여러 웹 스크래핑 작업을 동시에 생성하고 실행할 수 있으며 이벤트를 사용하여 응답과 오류를 처리할 수 있다.

예를 들어, 다음 코드는 `aiohttp`와 `aiofile`을 사용하여 해당 URL이 주어진 Wikipedia 기사의 제목과 요약을 스크랩하는 코루틴을 정의한다.

```python
# Import the asyncio, aiohttp, and aiofiles modules
import asyncio
import aiohttp
import aiofiles

# Define a coroutine that scrapes a Wikipedia article
async def scrape_article(url):
    # Create an asynchronous HTTP session
    async with aiohttp.ClientSession() as session:
        # Send an asynchronous GET request to the URL
        async with session.get(url) as response:
            # Check if the response status is OK
            if response.status == 200:
                # Read the response content as text
                html = await response.text()
                # Open an asynchronous file object
                async with aiofiles.open("article.html", "w") as file:
                    # Write the HTML content to the file
                    await file.write(html)
                    # Print a message
                    print(f"Saved {url} to article.html")
            else:
                # Print an error message
                print(f"Error: {response.status}")
    # Open another asynchronous file object
    async with aiofiles.open("article.html", "r") as file:
        # Read the HTML content from the file
        html = await file.read()
        # Parse the HTML content using BeautifulSoup
        soup = BeautifulSoup(html, "html.parser")
        # Find the title element
        title = soup.find(id="firstHeading")
        # Find the summary element
        summary = soup.find(id="mw-content-text").p
        # Print the title and the summary
        print(title.text)
        print(summary.text)
```

이 코루틴을 실행하기 위해서는 스크랩하고 싶은 위키피디아 기사의 URL을 전달하는 `asyncio.run()` 함수를 사용하면 된다. 예를 들어 다음 코드는 Python에 대한 위키피디아 기사의 URL로 코루틴을 실행한다.

```python
# Run the coroutine
asyncio.run(scrape_article("https://en.wikipedia.org/wiki/Python_(programming_language)"))
```

위의 코드를 실행하면 다음과 같은 결과를 출력한다.

```
Saved https://en.wikipedia.org/wiki/Python_(programming_language) to article.html
Python (programming language)
Python is an interpreted, high-level, general-purpose programming language. Created by Guido van Rossum and first released in 1991, Python's design philosophy emphasizes code readability with its notable use of significant indentation. Its language constructs and object-oriented approach aim to help programmers write clear, logical code for small and large-scale projects.[30]
```

위의 코드는 시연 목적으로만 사용되며 웹 스크래핑 중 발생할 수 있는 모든 오류와 예외를 처리하지는 않는다. 또한 서비스 약관이나 스크래핑 중인 웹사이트의 `robots.txt` 규칙을 위반하지 않도록 주의하고 요금 제한과 정책을 준수해야 한다.

<span style="color:red">다음 절에서는 웹 크롤링을 위해 Asyncio를 사용하는 방법에 대해 설명할 것인데, 이는 일반적으로 웹 검색, 웹 마이닝 또는 웹 아카이빙을 목적으로 웹 페이지를 체계적으로 탐색하고 인덱싱하는 과정이다. 그러나 이는 누락되어 언젠가 채워야 함.</span>

## <a name="summary"></a> 요약
이 포스팅에서는 코루틴, 태스크 및 이벤트를 사용하여 비동기 코드를 작성할 수 있는 모듈인 `Asyncio`를 사용하는 방법을 설명하였다.

- 비동기 프로그래밍이란 무엇이며, 데이터 분석 및 웹 개발에 유용한 이유
- Asyncio가 Python에서 작동하는 방식과 모듈의 주요 구성 요소
- `async`와 `await` 키워드를 사용한 코루틴의 생성과 실행
- 태스크를 사용한 코루틴의 동시 실행과 관리
- 이벤트를 사용한 이벤트 루프 제어와 콜백 처리
- 웹 스크래핑, 웹 크롤링, 웹 소켓 같은 일반적인 시나리오에서 Asyncio 적용 방법

이제 여러분은 효율적이고 확장 가능한 비동기 코드를 Python에서 작성하기 위한 Asyncio를 사용하는 방법에 대한 탄탄한 능력를 얻었다.

<a name="footnote_1">1</a>: [파이썬의 Future 클래스](https://medium.com/humanscape-tech/%ED%8C%8C%EC%9D%B4%EC%8D%AC%EC%9D%98-future-%ED%81%B4%EB%9E%98%EC%8A%A4-8b6bc15bd6af)
