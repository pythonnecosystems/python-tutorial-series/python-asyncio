# Python Asyncio: 코루틴, 태스크 및 이벤트 <sup>[1](#footnote_1)</sup>

> <font size="3">Python에서 비동기 모듈을 사용하여 비동기 코드를 작성하는 방법에 대해 알아본다.</font>

## 목차

1. [개요](./asyncio.md#intro)
1. [비동기 프로그래밍이란?](./asyncio.md#sec_02)
1. [Python에서 Asyncio 작동 방식](./asyncio.md#sec_03)
1. [코루틴: Asyncio의 기본 구성요소](./asyncio.md#sec_04)
1. [태스크: 코루틴 동시 실행](./asyncio.md#sec_05)
1. [이벤트: 이벤트 루프와 콜백 관리](./asyncio.md#sec_06)
1. [예: 일반적인 시나리오에서 Asyncio 사용](./asyncio.md#sec_07)
1. [요약](./asyncio.md#summary)

<a name="footnote_1">1</a>: [Python Tutorial 43 — Python Asyncio: Coroutines, Tasks, and Events](https://python.plainenglish.io/python-tutorial-43-python-asyncio-coroutines-tasks-and-events-8e8c7e99d37d?sk=9e93bcf87e22ff0c00fbc9f27bd87415)를 편역하였습니다.
